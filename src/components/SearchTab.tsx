import {TouchableOpacity} from 'react-native-gesture-handler';
import React from 'react';
import {Image, StyleSheet, View} from 'react-native';
import {useStores} from '../models';
import {SearchKeyword} from './SearchKeyword';
import {image} from '../themes';

export const SearchTab = ({
  keyword,
  setKeyword,
  isDatePicked,
  setShowDatePicker,
  onResetDate,
  isShowList,
  healthRecordId,
  healthRecordName,
  showIconDate = true,
  txPlacehoder,
}: any) => {
  const {user} = useStores();
  return (
    <View style={styles.container}>
      <SearchKeyword
        containerStyle={{flex: 1}}
        keyword={keyword}
        onChangeText={setKeyword}
        placeHolder={txPlacehoder}
      />
      {showIconDate && (
        <TouchableOpacity
          style={styles.btnCalendar}
          onPress={() => {
            isDatePicked ? onResetDate() : setShowDatePicker(true);
          }}>
          <Image
            style={styles.icoSmall}
            source={isDatePicked ? image.reload : image.calendar2}
          />
        </TouchableOpacity>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 16,
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  btnCalendar: {
    padding: 7,
    marginLeft: 8,
  },
  icoSmall: {
    width: 20,
    height: 20,
  },
});
