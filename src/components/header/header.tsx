import {View, Text, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import {image, scale} from '../../themes';
import {useNavigation} from '@react-navigation/native';
import {SvgIcon} from '../../themes/svg-icon';

const Header = (props: any) => {
  const navigate = useNavigation();
  const {iconBack, title} = props;
  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          paddingVertical: scale(5),

          marginHorizontal: 16,
        }}>
        {!!iconBack && (
          <TouchableOpacity
            style={{marginRight: 20}}
            onPress={() => navigate.goBack()}>
            <Image
              source={image.back}
              style={{
                width: 24,
                height: 24,
              }}
            />
          </TouchableOpacity>
        )}
        <Text
          numberOfLines={1}
          ellipsizeMode="tail"
          style={{
            color: '#000000',
            fontSize: 16,
            lineHeight: 24,
            fontWeight: '600',
            marginLeft: 4,
            flex: 1,
          }}>
          {title}
        </Text>
      </View>
      <View style={{borderBottomWidth: 1, borderBottomColor: '#E2E4E7'}} />
    </View>
  );
};

export default Header;
