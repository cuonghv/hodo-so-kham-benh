import React from 'react';
import {Image, StyleSheet, TextInput, View} from 'react-native';
import {image} from '../themes';

export const SearchKeyword = ({
  keyword,
  onChangeText,
  placeHolder,
  containerStyle,
}: any) => {
  return (
    <View style={[styles.container, containerStyle]}>
      <Image source={image.search2} style={styles.icoNormal} />
      <TextInput
        clearButtonMode={'always'}
        style={styles.textInput}
        onChangeText={onChangeText}
        placeholder={placeHolder}
        placeholderTextColor={'#C4C4C4'}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderColor: '#20419B',
    borderWidth: 1,
    borderRadius: 16,
    backgroundColor: 'white',
    paddingHorizontal: 12,
    paddingVertical: 10,
    alignItems: 'center',
  },
  icoNormal: {
    width: 24,
    height: 24,
    marginRight: 10,
  },
  icoSmall: {
    width: 20,
    height: 20,
  },
  textInput: {
    flex: 1,
    color: 'black',
    fontSize: 13,
    fontWeight: '500',
    padding: 0,
  },
  icoX: {
    width: 8,
    height: 8,
  },
  btnClear: {
    padding: 4,
  },
  viewClear: {
    width: 15,
    height: 15,
    borderRadius: 15,
    backgroundColor: '#DEDEDE',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
