import * as React from 'react';
import {StyleSheet} from 'react-native';
import {observer} from 'mobx-react-lite';
import {Modalize, ModalizeProps} from 'react-native-modalize';
import {Portal} from 'react-native-portalize';
import {useTheme} from '@react-navigation/native';
import {Text, View} from 'react-native';
import {scale, scaleSize} from '../../themes';
import {useStores} from '../../models';

export interface SwipeModalProps extends ModalizeProps {
  heightRatio?: number;
  children?: React.ReactNode;
  modalRef: React.Ref<Modalize>;
  keyboardAvoidingOffset?: number;
}

export const SwipeModal = observer(function SwipeModal(props: SwipeModalProps) {
  const {setting} = useStores();
  const theme = useTheme();
  const styles = style(theme);
  const validHeightRatio =
    props.heightRatio && props.heightRatio > 0 && props.heightRatio < 1
      ? props.heightRatio
      : 0.3;

  return (
    <Portal>
      <Modalize
        childrenStyle={[
          styles.childrenStyle,
          {paddingBottom: setting.insets.bottom},
        ]}
        // modalHeight={height * validHeightRatio}
        adjustToContentHeight
        disableScrollIfPossible={false}
        // handleStyle={styles.handleStyle}
        handlePosition="inside"
        {...props}
        ref={props.modalRef}>
        {props.children}
      </Modalize>
    </Portal>
  );
});

const style = (theme: {dark?: boolean; colors: any}) =>
  StyleSheet.create({
    handleStyle: {
      width: scaleSize(89),
      height: scaleSize(5),
      color: theme.colors.lightGrey,
    },
    childrenStyle: {
      paddingTop: scale(5),
    },
  });
