import * as React from 'react';
import {
  StyleProp,
  ImageBackground,
  View,
  ViewStyle,
  Image,
  ImageStyle,
} from 'react-native';
import {observer} from 'mobx-react-lite';
import {SvgIcon} from '../../themes/svg-icon';

const LOADINGCONTAINER: ViewStyle = {
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  zIndex: 99,
};

const LOADINGBACKDROP: ImageStyle = {
  width: '100%',
  height: '100%',
  // opacity: 0.7,
};

const LOADINGCONTENTCONTAINER: ViewStyle = {
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  justifyContent: 'center',
  alignItems: 'center',
};

const LOADINGBOX = {
  padding: 10,
  backgroundColor: '#fff',
  borderRadius: 12,
};

const LOADING = {
  width: 64,
  height: 64,
};

const LOGOBOX: ViewStyle = {
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  justifyContent: 'center',
  alignItems: 'center',
};

export interface LoadingProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>;
  noShadows?: boolean;
}

/**
 * Describe your component here
 */
export const Loading = observer(function Loading(props: LoadingProps) {
  return (
    <View style={LOADINGCONTAINER}>
      {!props?.noShadows && (
        <ImageBackground
          style={LOADINGBACKDROP}
          source={require('./backdrop.png')}
        />
      )}
      <View style={LOADINGCONTENTCONTAINER}>
        <View style={LOADINGBOX}>
          <Image style={LOADING} source={require('./loading.gif')} />
          <View style={LOGOBOX}>
            <SvgIcon icon="hodo" size={LOADING.width / 2} />
          </View>
        </View>
      </View>
    </View>
  );
});
