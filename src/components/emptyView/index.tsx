import React, {memo} from 'react';
import {ImageStyle, StyleSheet, Text, View, ViewStyle} from 'react-native';
// import {SvgIcon} from '../svg-icon/svg-icon';
import {scaleWidth, scaleHeight, scale} from '../../themes';
import {SvgIcon} from '../../themes/svg-icon';

interface EmptyViewProps {
  title: string;
  image?: any;
  imageStyle?: ImageStyle;
  containerStyle?: ViewStyle;
}

const EmptyView: React.FC<EmptyViewProps> = ({title, containerStyle}) => {
  return (
    <View style={[styles.container, containerStyle]}>
      <SvgIcon
        icon="listEmpty"
        width={scaleWidth(70)}
        height={scaleHeight(30)}
      />
      <Text style={[styles.title]}>{title}</Text>
    </View>
  );
};

export default memo(EmptyView);

const styles = StyleSheet.create({
  container: {
    marginTop: '18%',
    alignItems: 'center',
  },
  title: {
    fontWeight: '400',
    fontSize: scale(18),
    marginTop: 10,
  },
});
