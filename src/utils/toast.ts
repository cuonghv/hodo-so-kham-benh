import {ToastShowParams} from 'react-native-toast-message';
import Toast from 'react-native-toast-message';

export function showToast(params: ToastShowParams) {
  Toast.show(params);
}

export function showErrorToast(params: ToastShowParams) {
  params.type = 'error';
  showToast(params);
}
