export const method = (MT: number) => {
  if (MT === 1) {
    return 'Trực tiếp';
  } else {
    return 'online';
  }
};

export const gender = (gd: number) => {
  if (gd === 1) {
    return 'Nam';
  } else {
    return 'Nữ';
  }
};

export const spliceValue = (tx: string, min: string, max: string) => {
  const value = tx.split(' ')[0];

  if (parseFloat(value) < parseFloat(min)) {
    return '#043BFF';
  }
  if (parseFloat(value) > parseFloat(max)) {
    return '#D91717';
  }
  return '#23262F';
};

export const checkFile = (file: string) => {
  const re = /(?:\.([^.]+))?$/;
  switch (re.exec(file)[1]?.toLowerCase()) {
    case 'pdf':
      return 'pdf';
    default:
      return 'link';
  }
};
