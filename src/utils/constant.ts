export const STT_HEALTH_RECORDS = {
  1: 'Chờ khám',
  2: 'Đang khám',
  3: 'Chờ kết quả',
  4: 'Chờ đọc kết quả',
  5: 'Đã khám xong',
  6: 'Hủy',
  7: 'Đợi gọi tiếp',
  8: 'Nhỡ lượt khám',
};

export enum EnumTypeHealthRecord {
  HEALTH_RECORD_ALL = 'health_record_all',
  HEALTH_RECORD_FOR_HSSK = 'health_record_for_HSSK',
}

export enum EnumEnv {
  DEV = 'dev',
  PRD = 'prd',
}
