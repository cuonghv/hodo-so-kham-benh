import {I18n} from 'i18n-js';

import en from './en.json';
import vi from './vi.json';

const i18n = new I18n();

i18n.defaultLocale = 'en';

i18n.locale = 'en';

i18n.translations = {
  en,
  vi,
};

export default i18n;
