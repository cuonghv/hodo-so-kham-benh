import {
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import _ from 'lodash';
import {observer} from 'mobx-react-lite';
import {useStores} from '../models';
import {SearchTab} from '../components/SearchTab';
import Header from '../components/header/header';
import {HealthRecordApi} from '../services/api/apiHealthRecord';
import {USER} from '../services/api/api-url';
import {CardHealthRecord} from './component/CardHealthRecord';
import {EnumTypeHealthRecord} from '../utils/constant';
import EmptyView from '../components/emptyView';
import {Loading} from '../components';

export const HealthRecords = observer(() => {
  const {healthRecord, setting} = useStores();
  const [keyword, setKeyword] = useState('');
  const [loading, setLoading] = useState(false);
  const [healthRecords, setHealthRecords] = useState([]);
  const [refreshing, setRefreshing] = useState<boolean>(false);

  const params = {
    page_num: 1,
    page_size: 1000,
    person_id: setting.personId,
    person_diseases_id: setting.personDiseasesId,
  };

  const onChangeKeyword = (text: string) => {
    const formattedQuery = text.toUpperCase();
    const filtered = healthRecord.healthRecordList.filter(
      (item: {
        workspace: {organization: {name: string}};
        clinic: {organization: {name: string}};
      }) =>
        setting.typeGetData === EnumTypeHealthRecord.HEALTH_RECORD_FOR_HSSK
          ? item?.workspace?.organization?.name
              ?.toUpperCase()
              .indexOf(formattedQuery) > -1
          : item?.clinic?.organization?.name
              ?.toUpperCase()
              .indexOf(formattedQuery) > -1,
    );
    setKeyword(text);
    setHealthRecords(filtered);
  };

  useEffect(() => {
    fetchInitialData();
  }, []);

  const fetchInitialData = async () => {
    if (setting.personId) {
      try {
        setLoading(true);
        const response =
          setting.typeGetData === EnumTypeHealthRecord.HEALTH_RECORD_FOR_HSSK
            ? await HealthRecordApi({
                url: USER.getMedicalBooksGo(params),
                method: 'get',
                token: setting.token,
              })
            : await HealthRecordApi({
                url: USER.getMedicalBooks(),
                method: 'get',
                token: setting.token,
              });
        if (response) {
          setHealthRecords(response?.data?.data);
          healthRecord.setHealthRecordList(response?.data?.data);
        }
      } catch (err) {
        console.log('ERROR', err);
      } finally {
        setLoading(false);
      }
    } else {
      healthRecord.setHealthRecordList([]);
    }
  };

  return (
    <>
      {loading && !refreshing ? (
        <Loading />
      ) : (
        <SafeAreaView style={styles.root}>
          <Header title="Sổ khám bệnh" />
          {healthRecord.healthRecordList?.length > 0 ? (
            <View style={{flex: 1, backgroundColor: '#F0F3FC'}}>
              <SearchTab
                keyword={keyword}
                txPlacehoder={'Nhập nơi khám'}
                setKeyword={onChangeKeyword}
                showIconDate={false}
              />
              {healthRecord.healthRecordList &&
                healthRecord.healthRecordList?.length > 0 &&
                healthRecord.healthRecordList?.data !== null && (
                  <ScrollView
                    style={styles.containerScrollView}
                    showsVerticalScrollIndicator={false}
                    refreshControl={
                      <RefreshControl
                        refreshing={refreshing}
                        onRefresh={async () => {
                          try {
                            setRefreshing(true);
                            await fetchInitialData();
                          } finally {
                            setRefreshing(false);
                          }
                        }}
                      />
                    }>
                    {healthRecords?.map(item => {
                      return (
                        <View style={{marginVertical: 8}} key={item?.id}>
                          <CardHealthRecord item={item} />
                        </View>
                      );
                    })}

                    {!(healthRecords.length > 0) && (
                      <EmptyView title={'Không tìm thấy sổ khám nào'} />
                    )}
                    <View style={styles.marginBot} />
                  </ScrollView>
                )}
            </View>
          ) : (
            <EmptyView title={'Không tìm thấy sổ khám nào'} />
          )}
        </SafeAreaView>
      )}
    </>
  );
});

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  marginBot: {
    marginBottom: 72,
  },
  containerScrollView: {
    marginHorizontal: 16,
    flex: 1,
  },
});
