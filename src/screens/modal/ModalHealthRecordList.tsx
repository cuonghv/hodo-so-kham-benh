import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  FlatList,
} from 'react-native';
import React from 'react';
import {Modalize} from 'react-native-modalize';
import {observer} from 'mobx-react-lite';
import moment from 'moment';
import {TypeModal} from '../../types/types';
import {useStores} from '../../models';
import {EnumTypeHealthRecord} from '../../utils/constant';
import {scale} from '../../themes';
import {SwipeModal} from '../../components/swipe-modal/swipe-modal';
import {SvgIcon} from '../../themes/svg-icon';

export interface IProps {
  modalRef: React.Ref<Modalize>;
  onClosed?: () => void;
  typeModal?: TypeModal;
}

const {height} = Dimensions.get('window');

const ModalHealthRecordList = observer((props: IProps) => {
  const {healthRecord, setting} = useStores();

  const Infor = () => {
    return (
      <View style={styles.root}>
        {props?.typeModal === 'so_kham' ? (
          <FlatList
            showsVerticalScrollIndicator={false}
            data={healthRecord.healthRecordList}
            keyExtractor={item => item.id}
            renderItem={({item}) => {
              return (
                <TouchableOpacity
                  style={styles.containerListHealthRecord}
                  onPress={() => {
                    healthRecord.setChooseHealthRecord(item);
                    props.onClosed();
                  }}
                  key={item.id}>
                  <Text
                    style={
                      healthRecord.chooseHealthRecord === item
                        ? styles.txContent2
                        : styles.txContent
                    }>
                    {item?.workspace?.organization?.name ||
                      item?.clinic?.organization?.name}
                  </Text>
                  <SvgIcon
                    icon={
                      healthRecord.chooseHealthRecord === item
                        ? 'radio_button_check'
                        : 'radio_button_uncheck'
                    }
                    size={24}
                  />
                </TouchableOpacity>
              );
            }}
          />
        ) : (
          <FlatList
            showsVerticalScrollIndicator={false}
            data={healthRecord.healthRecordDetail}
            keyExtractor={item => item.id}
            renderItem={({item}) => {
              return (
                <TouchableOpacity
                  style={styles.containerListHealthRecord}
                  onPress={() => {
                    healthRecord.setChooseHealthRecordDetail(item);
                    props.onClosed();
                  }}
                  key={item.id}>
                  <View>
                    <Text
                      style={
                        healthRecord.chooseHealthRecordDetail === item
                          ? styles.txContent2
                          : styles.txContent
                      }>
                      {'Ngày khám'}{' '}
                      {setting.typeGetData ===
                      EnumTypeHealthRecord.HEALTH_RECORD_FOR_HSSK ? (
                        <>
                          {moment(item?.patient_visit?.start_time)
                            .utc()
                            .format('HH:mm - DD/MM/YYYY')}
                        </>
                      ) : (
                        <>
                          {moment(item?.start_time).format(
                            'HH:mm - DD/MM/YYYY',
                          )}
                        </>
                      )}
                    </Text>
                    {(!item?.patient_visit?.doctor?.user?.name ||
                      !!item?.doctor_ref_name ||
                      item?.patient_visit?.doctor_ref_name) && (
                      <View style={{flexDirection: 'row'}}>
                        <Text
                          style={
                            healthRecord.chooseHealthRecordDetail === item
                              ? styles.txSubContent2
                              : styles.txSubContent
                          }>
                          {'Bác sĩ'}:{' '}
                        </Text>
                        <Text
                          style={
                            healthRecord.chooseHealthRecordDetail === item
                              ? styles.txSubContent2
                              : styles.txSubContent
                          }>
                          {item?.doctor_ref_name ||
                            item?.patient_visit?.doctor?.user?.name ||
                            item?.patient_visit?.doctor_ref_name}
                        </Text>
                      </View>
                    )}

                    {!!item?.patient_visit?.person?.name && (
                      <View style={{flexDirection: 'row'}}>
                        <Text
                          style={
                            healthRecord.chooseHealthRecordDetail === item
                              ? styles.txSubContent2
                              : styles.txSubContent
                          }>
                          {'Bệnh nhân'}:{' '}
                        </Text>
                        <Text
                          style={
                            healthRecord.chooseHealthRecordDetail === item
                              ? styles.txSubContent2
                              : styles.txSubContent
                          }>
                          {item?.patient_visit?.person?.name}
                        </Text>
                      </View>
                    )}
                  </View>

                  <SvgIcon
                    icon={
                      healthRecord.chooseHealthRecordDetail === item
                        ? 'radio_button_check'
                        : 'radio_button_uncheck'
                    }
                    size={24}
                  />
                </TouchableOpacity>
              );
            }}
          />
        )}
      </View>
    );
  };

  return (
    <SwipeModal
      modalRef={props.modalRef}
      childrenStyle={{marginVertical: scale(12), padding: 0}}
      HeaderComponent={
        <View style={styles.header}>
          <Text style={styles.title}>
            {props?.typeModal === 'so_kham'
              ? 'Danh sách sổ khám bệnh'
              : 'Danh sách phiên khám'}
          </Text>
        </View>
      }
      children={Infor()}
      FooterComponent={<View style={{height: scale(30)}} />}
    />
  );
});

export default ModalHealthRecordList;

const styles = StyleSheet.create({
  containerListHealthRecord: {
    marginBottom: scale(10),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  txSubContent: {
    fontSize: 15,
    lineHeight: 20,
    fontWeight: '400',
    color: '#23262F',
  },
  txSubContent2: {
    fontSize: 15,
    lineHeight: 20,
    fontWeight: '400',
    color: '#20419B',
  },
  txContent: {
    fontSize: 16,
    lineHeight: 20,
    fontWeight: '600',
    color: '#23262F',
  },
  txContent2: {
    fontSize: 16,
    lineHeight: 20,
    fontWeight: '600',
    color: '#20419B',
  },
  root: {
    flex: 1,
    maxHeight: height / 2,
    marginHorizontal: scale(16),
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: scale(20),
    padding: scale(12),
    borderBottomWidth: 1,
    borderColor: '#E2E4E7',
  },
  title: {
    fontSize: 20,
    fontWeight: '700',
    lineHeight: 24,
  },
  flex1: {
    flex: 1,
  },
  marginBot: {
    marginBottom: scale(12),
  },
  container: {
    backgroundColor: '#fff',
    marginHorizontal: scale(16),
    marginVertical: scale(8),
    padding: scale(8),
    borderRadius: 12,
    paddingHorizontal: scale(12),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 1,
  },
  row: {
    flexDirection: 'row',
    marginVertical: scale(4),
  },
});
