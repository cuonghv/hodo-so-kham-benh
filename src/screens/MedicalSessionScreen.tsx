import {StyleSheet, ScrollView, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import _ from 'lodash';
import {observer} from 'mobx-react-lite';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useStores} from '../models';
import {EnumTypeHealthRecord} from '../utils/constant';
import {Loading} from '../components';
import SelectPatientVisit from './component/sectionHealthRecord/SelectPatientVisit';
import Info from './component/sectionHealthRecord/Info';
import Prescription from './component/sectionHealthRecord/Prescription';
import Assay from './component/sectionHealthRecord/Assay';
import DiagnoseYourImage from './component/sectionHealthRecord/DiagnoseYourImage';
import OtherIndications from './component/sectionHealthRecord/OtherIndications';
import MolecularBiology from './component/sectionHealthRecord/MolecularBiology';
import ReferenceDocument from './component/sectionHealthRecord/ReferenceDocument';
import EmptyView from '../components/emptyView';
import {USER} from '../services/api/api-url';
import {HealthRecordApi} from '../services/api/apiHealthRecord';
import Header from './component/sectionHealthRecord/Header';

const MedicalSession = observer(() => {
  const {setting, healthRecord} = useStores();
  const [loadingList, setLoadingList] = useState(false);
  const [loadingDetail, setLoadingDetail] = useState(false);
  const [selectPatientVisit, setSelectPatientVisit] = useState([]);

  const params = {
    page_num: 1,
    page_size: 1000,
    person_id: setting.personId,
    person_diseases_id: setting.personDiseasesId,
  };

  // get listmedical session
  useEffect(() => {
    (async () => {
      if (healthRecord.chooseHealthRecord.id) {
        try {
          setLoadingList(true);
          const response =
            setting.typeGetData === EnumTypeHealthRecord.HEALTH_RECORD_FOR_HSSK
              ? await HealthRecordApi({
                  url: USER.getDetailMedicalBookGo(params),
                  method: 'get',
                  token: setting.token,
                })
              : await HealthRecordApi({
                  url: USER.getDetailMedicalBook(
                    healthRecord.chooseHealthRecord.id,
                  ),
                  method: 'get',
                  token: setting.token,
                });

          if (response && response?.data) {
            setSelectPatientVisit(
              setting.typeGetData ===
                EnumTypeHealthRecord.HEALTH_RECORD_FOR_HSSK
                ? response?.data?.data?.data?.reverse()
                : response?.data?.data?.patient_visist?.reverse(),
            );
            healthRecord.setHealthRecordDetail(
              setting.typeGetData ===
                EnumTypeHealthRecord.HEALTH_RECORD_FOR_HSSK
                ? response?.data?.data?.data?.reverse()
                : response?.data?.data?.patient_visist?.reverse(),
            );
            healthRecord.setChooseHealthRecordDetail(
              setting.typeGetData ===
                EnumTypeHealthRecord.HEALTH_RECORD_FOR_HSSK
                ? response?.data?.data?.data?.[0]
                : response?.data?.data?.patient_visist?.[0],
            );
          }
        } catch (err) {
          console.log('ERROR', err);
        } finally {
          setLoadingList(false);
        }
      } else {
        healthRecord.setHealthRecordDetail([]);
        healthRecord.setChooseHealthRecordDetail({});
        setLoadingList(false);
      }
    })();
  }, [
    healthRecord.chooseHealthRecord,
    healthRecord.chooseHealthRecord.person_id,
  ]);

  // Get medical session detail
  useEffect(() => {
    (async () => {
      if (healthRecord.chooseHealthRecordDetail) {
        try {
          setLoadingDetail(true);
          const response = await HealthRecordApi({
            url: USER.getPatientVisitDetailGo(
              setting.typeGetData ===
                EnumTypeHealthRecord.HEALTH_RECORD_FOR_HSSK
                ? healthRecord.chooseHealthRecordDetail.patient_visit_id
                : healthRecord.chooseHealthRecordDetail?.id,
            ),
            method: 'get',
            token: setting.token,
          });

          if (response && response.data) {
            healthRecord.setConsultation(response.data?.consultation_list);
            healthRecord.setPrescription(response.data?.presciption);
            healthRecord.setAssay(response.data?.indication_exam);
            healthRecord.setDiagnoseYourImage(response.data?.indication_pacs);
            healthRecord.setOtherIndication(response.data?.indication_other);
            healthRecord.setMolecularBiology(
              response.data?.indication_service_local,
            );
            healthRecord.setFile(response.data?.file_list);
          }
        } catch (err) {
          console.log('ERROR', err);
        } finally {
          setLoadingList(false);
        }
      } else {
        setLoadingList(false);
        healthRecord.setConsultation([]);
        healthRecord.setPrescription([]);
        healthRecord.setAssay([]);
        healthRecord.setDiagnoseYourImage([]);
        healthRecord.setOtherIndication([]);
        healthRecord.setMolecularBiology([]);
        healthRecord.setFile([]);
      }
    })();
  }, [healthRecord.chooseHealthRecordDetail]);

  return (
    <View style={{backgroundColor: '#ffff', flex: 1}}>
      {loadingList && loadingDetail ? (
        <Loading />
      ) : (
        <SafeAreaView style={styles.root}>
          <Header />
          {selectPatientVisit?.length > 0 && <SelectPatientVisit />}
          <ScrollView style={styles.flex1} showsVerticalScrollIndicator={false}>
            {healthRecord.consultation?.length > 0 && <Info />}
            {healthRecord.prescription?.length > 0 && <Prescription />}
            {healthRecord.assay?.length > 0 && <Assay />}
            {healthRecord.diagnoseYourImage?.length > 0 && (
              <DiagnoseYourImage />
            )}
            {healthRecord.otherIndications?.length > 0 && <OtherIndications />}
            {healthRecord.molecularBiology?.length > 0 && <MolecularBiology />}
            {healthRecord.files?.length > 0 && <ReferenceDocument />}

            {/* no data */}
            {!(
              healthRecord.consultation?.length > 0 ||
              healthRecord.prescription?.length > 0 ||
              healthRecord.assay?.length > 0 ||
              healthRecord.diagnoseYourImage?.length > 0 ||
              healthRecord.otherIndications?.length > 0 ||
              healthRecord.molecularBiology?.length > 0
            ) && <EmptyView title={'Không có dữ liệu phiên khám'} />}
          </ScrollView>
        </SafeAreaView>
      )}
    </View>
  );
});

export default MedicalSession;

const styles = StyleSheet.create({
  flex1: {
    flex: 1,
  },
  root: {marginHorizontal: 16, flex: 1},
});
