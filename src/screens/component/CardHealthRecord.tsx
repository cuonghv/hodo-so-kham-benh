import {StyleSheet, Text, TouchableOpacity, View, Image} from 'react-native';
import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {observer} from 'mobx-react-lite';
import {EnumTypeHealthRecord} from '../../utils/constant';
import {useStores} from '../../models';
import {image, scale} from '../../themes';
import {Routes} from '../../helper';

interface IProps {
  item: any;
}

export const CardHealthRecord = observer((props: IProps) => {
  const navigate = useNavigation();
  const {healthRecord, setting} = useStores();

  return (
    <TouchableOpacity
      onPress={() => {
        navigate.navigate(Routes.DETAIL);
        healthRecord.setChooseHealthRecord(props.item);
      }}
      style={styles.root}>
      <View style={styles.row}>
        <Image
          source={
            !!props?.item?.clinic?.avatar
              ? setting.environment.api.getImageUri(
                  setting.typeGetData ===
                    EnumTypeHealthRecord.HEALTH_RECORD_FOR_HSSK
                    ? props?.item?.workspace?.organization?.logo
                    : props?.item?.clinic?.avatar,
                )
              : image.avatar_clinic_default
          }
          style={{
            height: 30,
            width: 30,
            borderRadius: 10,
          }}
        />
        <Text
          style={styles.txNameHospital}
          numberOfLines={2}
          ellipsizeMode="tail">
          {setting.typeGetData === EnumTypeHealthRecord.HEALTH_RECORD_FOR_HSSK
            ? props?.item?.workspace?.organization?.name
            : props?.item?.clinic?.organization?.name}
        </Text>
      </View>
    </TouchableOpacity>
  );
});

const styles = StyleSheet.create({
  txNameHospital: {
    fontWeight: '600',
    fontSize: 16,
    alignSelf: 'center',
    marginHorizontal: scale(8),
    lineHeight: 20,
    paddingRight: scale(5),
    flex: 1,
  },
  root: {
    backgroundColor: '#fff',
    borderRadius: 12,
    paddingHorizontal: scale(16),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 1,
    elevation: 1,
  },
  row: {
    flexDirection: 'row',
    paddingVertical: scale(16),
    alignItems: 'center',
  },
});
