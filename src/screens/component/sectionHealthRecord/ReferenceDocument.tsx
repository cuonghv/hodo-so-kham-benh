import {StyleSheet, Text, View, TouchableOpacity, Linking} from 'react-native';
import React, {useState} from 'react';
import {observer} from 'mobx-react-lite';
import {useNavigation} from '@react-navigation/native';
import {useStores} from '../../../models';
import {checkFile} from '../../../utils/healthReacord';
import {SvgIcon} from '../../../themes/svg-icon';
import {scale} from '../../../themes';
import {Routes} from '../../../helper';

const ReferenceDocument = observer(() => {
  const {
    healthRecord: {files},
  } = useStores();
  const [more, setMore] = useState(false);
  const navigation = useNavigation();

  const handleOpenLink = (link, title) => {
    if (link) {
      checkFile(link) === 'pdf'
        ? navigation.navigate(Routes.VIEW_PDF, {link, title})
        : Linking.openURL(link);
    }
  };

  return (
    <>
      <View>
        <TouchableOpacity
          style={more ? styles.container2 : styles.container}
          onPress={() => setMore(!more)}>
          <Text style={styles.txTitle}>{'Tài liệu tham khảo'}</Text>
          <SvgIcon icon={more ? 'arrowDown' : 'arrow_right_single'} size={14} />
        </TouchableOpacity>
        {more && (
          <>
            {files?.map(item => {
              return (
                <TouchableOpacity
                  onPress={() => handleOpenLink(item?.url, item?.name)}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginRight: scale(16),
                      marginTop: scale(10),
                    }}>
                    <SvgIcon icon="view" size={24} />
                    <Text style={styles.txNameAssay}>
                      {item?.name || item?.url}
                    </Text>
                  </View>
                </TouchableOpacity>
              );
            })}
          </>
        )}
      </View>
      <View style={more ? styles.boderBotMore : styles.boderBot} />
    </>
  );
});

export default ReferenceDocument;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingRight: scale(8),
    alignItems: 'center',
    paddingVertical: scale(8),
  },
  boderBot: {
    borderWidth: 1,
    padding: 0,
    borderColor: '#C3CAE7',
  },
  row: {
    flexDirection: 'row',
    marginVertical: scale(4),
  },
  txContent: {
    fontSize: 14,
    fontWeight: '400',
    lineHeight: 20,
    flex: 1,
  },
  txNameAssay: {
    color: '#20419B',
    flex: 1,
    marginLeft: scale(8),
    fontWeight: '500',
    fontSize: 14,
    lineHeight: 21,
  },
  txTitle: {
    fontSize: 14,
    fontWeight: '600',
    lineHeight: 21,
    color: '#20419B',
  },
  boderBotMore: {
    borderWidth: 1,
    padding: 0,
    borderColor: '#C3CAE7',
    marginTop: scale(16),
  },
  container2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingRight: scale(8),
    alignItems: 'center',
    paddingTop: scale(8),
  },
});
