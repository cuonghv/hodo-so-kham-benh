import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useRef, useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import {observer} from 'mobx-react-lite';
import {useStores} from '../../../models';
import {TypeModal} from '../../../types/types';
import {image, scale} from '../../../themes';
import ModalHealthRecordList from '../../modal/ModalHealthRecordList';
import {SvgIcon} from '../../../themes/svg-icon';

const {width} = Dimensions.get('window');

const Header = observer(() => {
  const [typeModal, setTypeModal] = useState<TypeModal>();
  const modalListHealthRecord = useRef(null);
  const {healthRecord} = useStores();
  const navigate = useNavigation();

  return (
    <View>
      <View style={styles.containerHeader}>
        <TouchableOpacity
          style={{marginRight: 20}}
          onPress={() => navigate.goBack()}>
          <Image
            source={image.back}
            style={{
              width: 24,
              height: 24,
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.itemHeaderR}
          onPress={() => {
            modalListHealthRecord?.current?.open();
            setTypeModal('so_kham');
          }}>
          <Text
            numberOfLines={2}
            ellipsizeMode="tail"
            style={styles.txNameHospital}>
            {healthRecord.chooseHealthRecord?.workspace?.organization?.name ||
              healthRecord.chooseHealthRecord?.clinic.organization?.name}
          </Text>
          <SvgIcon icon="arrowRight" size={16} />
        </TouchableOpacity>
      </View>
      <View style={styles.boderBot} />

      <ModalHealthRecordList
        modalRef={modalListHealthRecord}
        onClosed={() => modalListHealthRecord?.current?.close()}
        typeModal={typeModal}
      />
    </View>
  );
});

export default Header;

const styles = StyleSheet.create({
  containerHeader: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  itemHeaderL: {
    marginLeft: scale(-14),
  },
  itemHeaderR: {
    flexDirection: 'row',
    paddingVertical: scale(6),
    justifyContent: 'center',
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#F0F3FC',
    borderRadius: 8,
    paddingHorizontal: scale(8),
  },
  txNameHospital: {
    fontWeight: '600',
    fontSize: 16,
    alignSelf: 'center',
    marginLeft: scale(8),
    lineHeight: 24,
    flex: 1,
  },
  boderBot: {
    borderBottomWidth: 1,
    width: width,
    marginLeft: scale(-16),
    padding: 0,
    borderColor: '#F0F3FC',
    marginTop: scale(8),
  },
});
