import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React, {useState} from 'react';
import {observer} from 'mobx-react-lite';
import RenderHtml from 'react-native-render-html';
import {useStores} from '../../../models';
import {SvgIcon} from '../../../themes/svg-icon';
import {scale} from '../../../themes';

const Info = observer(() => {
  const {
    healthRecord: {consultation},
  } = useStores();
  const [more, setMore] = useState(false);

  return (
    <>
      <View>
        <TouchableOpacity
          style={styles.container}
          onPress={() => setMore(!more)}>
          <Text style={styles.txTitle}>{'Phiếu khám'}</Text>
          <SvgIcon icon={more ? 'arrowDown' : 'arrow_right_single'} size={14} />
        </TouchableOpacity>
        {more && (
          <>
            <View>
              <RenderHtml
                source={{
                  html: consultation?.[0]?.content,
                }}
              />
            </View>
          </>
        )}
      </View>
      <View style={more ? styles.boderBotMore : styles.boderBot} />
    </>
  );
});

export default Info;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingRight: scale(8),
    alignItems: 'center',
    paddingVertical: scale(8),
  },
  boderBot: {
    borderWidth: 1,
    padding: 0,
    borderColor: '#C3CAE7',
  },
  row: {
    flexDirection: 'row',
    marginVertical: scale(4),
  },
  txContent: {
    fontSize: 14,
    fontWeight: '400',
    lineHeight: 20,
    flex: 1,
  },
  txSubTitle: {
    fontSize: 16,
    fontWeight: '600',
    lineHeight: 20,
  },
  txTitle: {
    fontSize: 14,
    fontWeight: '600',
    lineHeight: 21,
    color: '#20419B',
  },
  boderBotMore: {
    borderWidth: 1,
    padding: 0,
    borderColor: '#C3CAE7',
    marginTop: scale(16),
  },
});
