import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React, {useState} from 'react';
import {scale} from '../../../themes';
import {SvgIcon} from '../../../themes/svg-icon';

interface IProps {
  item?: any;
}

const PrescriptionItem = (props: IProps) => {
  const [more, setMore] = useState(false);
  const {item} = props;

  return (
    <View>
      <TouchableOpacity
        style={more ? styles.container2 : styles.container}
        onPress={() => setMore(!more)}>
        <Text style={styles.txTitle}>{item?.name || 'Đơn thuốc'}</Text>
        <SvgIcon icon={more ? 'arrowDown' : 'arrow_right_single'} size={14} />
      </TouchableOpacity>
      {more && item?.person_medicine && (
        <>
          <View style={styles.containerItem}>
            <View style={styles.item1}>
              <Text style={styles.containerValue1Title}>{'Tên thuốc'}</Text>
            </View>
            <View style={styles.containerValue}>
              <Text style={styles.containerValue2Title}>{'Số lượng'}</Text>
            </View>
          </View>
          <>
            {item?.person_medicine?.map(_detail => {
              return (
                <View style={{marginVertical: scale(5)}}>
                  <View style={styles.containerItem}>
                    <View style={styles.item1}>
                      <Text style={styles.containerValue1}>
                        {_detail?.name}
                      </Text>
                    </View>
                    <View style={styles.containerValue}>
                      <Text style={styles.containerValue2}>
                        {_detail?.amount} {_detail?.amount_unit}
                      </Text>
                    </View>
                  </View>
                  {!!_detail?.doctor_note && (
                    <View style={styles.wrapGuid}>
                      <Text style={styles.txGuid}>{'instruction'}: </Text>
                      <Text style={styles.txGuid2}>{_detail?.doctor_note}</Text>
                    </View>
                  )}
                  {!!_detail?.side_effect && (
                    <View style={styles.wrapGuid}>
                      <Text style={styles.txGuid}>{'Tác dụng phụ'}: </Text>
                      <Text style={styles.txGuid2}>{_detail?.side_effect}</Text>
                    </View>
                  )}
                </View>
              );
            })}
          </>
        </>
      )}
    </View>
  );
};

export default PrescriptionItem;

const styles = StyleSheet.create({
  wrapGuid: {
    flexDirection: 'row',
    paddingRight: scale(8),
    flex: 1,
  },
  txGuid: {
    fontSize: 14,
    fontWeight: '500',
    lineHeight: 21,
    color: '#000000',
  },
  txGuid2: {
    fontSize: 14,
    fontWeight: '400',
    lineHeight: 21,
    color: '#000000',
    flex: 1,
    marginRight: '14%',
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingRight: scale(8),
    alignItems: 'center',
    paddingVertical: scale(8),
  },
  container2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingRight: scale(8),
    alignItems: 'center',
    paddingTop: scale(8),
  },

  row: {
    flexDirection: 'row',
    marginVertical: scale(4),
  },
  txContent: {
    fontSize: 14,
    fontWeight: '400',
    lineHeight: 20,
    flex: 1,
  },
  txSubTitle: {
    fontSize: 16,
    fontWeight: '600',
    lineHeight: 20,
  },
  txTitle: {
    fontSize: 14,
    fontWeight: '600',
    lineHeight: 21,
    color: '#20419B',
  },
  containerItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  item1: {
    justifyContent: 'center',
    width: '80%',
  },
  containerValue1: {
    justifyContent: 'center',
    fontWeight: '400',
    fontSize: 14,
    lineHeight: 20,
    color: '#23262F',
  },
  containerValue2: {
    justifyContent: 'center',
    fontWeight: '400',
    fontSize: 14,
    lineHeight: 20,
    textAlign: 'center',
    color: '#23262F',
  },
  containerValue: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerValue1Title: {
    justifyContent: 'center',
    fontWeight: '600',
    fontSize: 14,
    lineHeight: 20,
    color: '#23262F',
  },
  containerValue2Title: {
    justifyContent: 'center',
    fontWeight: '600',
    fontSize: 14,
    lineHeight: 20,
    textAlign: 'center',
    color: '#23262F',
  },
});
