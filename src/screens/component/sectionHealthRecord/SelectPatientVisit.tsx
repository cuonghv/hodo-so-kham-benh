import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React, {useRef, useState} from 'react';
import {observer} from 'mobx-react-lite';
import {useStores} from '../../../models';
import {scale} from '../../../themes';
import ModalHealthRecordList from '../../modal/ModalHealthRecordList';
import {useNavigation} from '@react-navigation/native';
import {TypeModal} from '../../../types/types';
import {SvgIcon} from '../../../themes/svg-icon';
import {EnumTypeHealthRecord} from '../../../utils/constant';
import moment from 'moment';

const SelectPatientVisit = observer(() => {
  const navigate = useNavigation();
  const [typeModal, setTypeModal] = useState<TypeModal>();
  const modalListHealthRecord = useRef(null);
  const {healthRecord, setting} = useStores();

  return (
    <View style={{marginTop: scale(8)}}>
      <View style={styles.containerClender}>
        <TouchableOpacity
          style={styles.btnClender}
          onPress={() => {
            modalListHealthRecord?.current?.open();
            setTypeModal('phien_kham');
          }}>
          <View style={styles.containerR}>
            <SvgIcon icon="calendar_month" size={24} />
            <Text style={styles.txNgayKham}>{'Ngày khám '}</Text>
            <Text style={styles.txTimeKham}>
              {setting.typeGetData ===
              EnumTypeHealthRecord.HEALTH_RECORD_ALL ? (
                <>
                  {moment(
                    setting.typeGetData ===
                      EnumTypeHealthRecord.HEALTH_RECORD_FOR_HSSK
                      ? healthRecord.chooseHealthRecordDetail?.patient_visit
                          ?.start_time
                      : healthRecord.chooseHealthRecordDetail?.start_time,
                  ).format('HH:mm - DD/MM/YYYY')}
                </>
              ) : (
                <>
                  {moment(
                    setting.typeGetData ===
                      EnumTypeHealthRecord.HEALTH_RECORD_FOR_HSSK
                      ? healthRecord.chooseHealthRecordDetail?.patient_visit
                          ?.start_time
                      : healthRecord.chooseHealthRecordDetail?.start_time,
                  )
                    .utc()
                    .format('HH:mm - DD/MM/YYYY')}
                </>
              )}
            </Text>
          </View>
          <SvgIcon icon="arrowRight" size={16} />
        </TouchableOpacity>
      </View>

      <ModalHealthRecordList
        modalRef={modalListHealthRecord}
        onClosed={() => modalListHealthRecord?.current?.close()}
        typeModal={typeModal}
      />
    </View>
  );
});

export default SelectPatientVisit;

const styles = StyleSheet.create({
  containerR: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  containerClender: {
    marginBottom: 8,
    backgroundColor: '#F0F3FC',
    paddingHorizontal: scale(8),
    paddingVertical: scale(4),
    borderRadius: 8,
  },
  txTimeKham: {
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 20,
    color: '#20419B',
  },
  txNgayKham: {
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 20,
    color: '#23262F',
    marginLeft: scale(8),
  },

  btnClender: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: scale(8),
    alignItems: 'center',
  },
});
