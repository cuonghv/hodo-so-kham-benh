import {StyleSheet, Text, View, TouchableOpacity, Linking} from 'react-native';
import React, {useState} from 'react';
import {observer} from 'mobx-react-lite';
import {useNavigation} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {useStores} from '../../../models';
import {checkFile, spliceValue} from '../../../utils/healthReacord';
import {SvgIcon} from '../../../themes/svg-icon';
import {scale} from '../../../themes';
import {Routes} from '../../../helper';

const Assay = observer(() => {
  const {
    healthRecord: {assay},
  } = useStores();
  const [more, setMore] = useState(false);
  const navigate = useNavigation();
  const handleOpenLink = (link: string, title: any) => {
    if (link) {
      checkFile(link) === 'pdf'
        ? navigate.navigate(Routes.VIEW_PDF, {link, title})
        : Linking.openURL(link);
    }
  };

  return (
    <>
      <TouchableOpacity
        style={more ? styles.container2 : styles.container}
        onPress={() => setMore(!more)}>
        <Text style={styles.txTitle}>{'Xét nghiệm'}</Text>
        <SvgIcon icon={more ? 'arrowDown' : 'arrow_right_single'} size={14} />
      </TouchableOpacity>

      <>
        {assay?.map(item => {
          return (
            <View key={item.id}>
              {more && item?.examination_result?.examination_result_values && (
                <>
                  <TouchableOpacity
                    onPress={() =>
                      handleOpenLink(item?.document_link, item?.name)
                    }>
                    <View
                      style={{
                        marginTop: scale(8),
                      }}>
                      <Text style={styles.txNameAssay}>{item?.name}</Text>
                    </View>
                  </TouchableOpacity>
                  {item?.examination_result?.examination_result_values?.length >
                    0 && (
                    <View style={{marginVertical: scale(5)}}>
                      <View style={styles.containerItem}>
                        <View style={styles.item1}>
                          <Text style={styles.containerValue1Title}>
                            {'Xét nghiệm'}
                          </Text>
                        </View>
                        <View style={styles.containerValue}>
                          <Text style={styles.containerValue2Title}>
                            {'Kết quả xét nghiệm'}
                          </Text>
                        </View>
                        <View style={styles.containerValue}>
                          <Text style={styles.containerValue2Title}>
                            {'Bình thường'}
                          </Text>
                        </View>
                      </View>
                    </View>
                  )}

                  <>
                    {item?.examination_result?.examination_result_values?.map(
                      _detail => {
                        return (
                          <View style={{marginVertical: scale(5)}}>
                            <View style={styles.containerItem}>
                              <View style={styles.item1}>
                                <Text style={styles.containerValue1}>
                                  {_detail?.name}
                                </Text>
                              </View>
                              <View style={styles.containerValue}>
                                <Text
                                  style={{
                                    justifyContent: 'center',
                                    fontWeight: '400',
                                    textAlign: 'center',
                                    fontSize: 14,
                                    lineHeight: 20,
                                    color: spliceValue(
                                      _detail?.value,
                                      _detail?.lower,
                                      _detail?.upper,
                                    ),
                                  }}>
                                  {_detail?.value} {_detail?.unit}
                                </Text>
                              </View>
                              <View style={styles.containerValue}>
                                <Text style={styles.containerValue2}>
                                  {_detail?.lower} - {_detail?.upper}
                                </Text>
                              </View>
                            </View>
                            {!!_detail?.note && (
                              <View style={styles.wrapGuid}>
                                <Text style={styles.txGuid}>{'Ghi chú'} </Text>
                                <Text style={styles.txGuid2}>
                                  {_detail?.note}
                                </Text>
                              </View>
                            )}
                          </View>
                        );
                      },
                    )}
                  </>
                </>
              )}
            </View>
          );
        })}
      </>
      <View style={more ? styles.boderBotMore : styles.boderBot} />
    </>
  );
});

export default Assay;

const styles = StyleSheet.create({
  wrapGuid: {
    flexDirection: 'row',
    paddingRight: scale(8),
    flex: 1,
  },
  txGuid: {
    fontSize: 14,
    fontWeight: '500',
    lineHeight: 21,
    color: '#000000',
  },
  txGuid2: {
    fontSize: 14,
    fontWeight: '400',
    lineHeight: 21,
    color: '#000000',
    flex: 1,
  },
  txNameAssay: {
    color: '#20419B',
    flex: 1,
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingRight: scale(8),
    alignItems: 'center',
    paddingVertical: scale(8),
  },
  container2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingRight: scale(8),
    alignItems: 'center',
    paddingTop: scale(8),
  },
  boderBot: {
    borderWidth: 1,
    padding: 0,
    borderColor: '#C3CAE7',
  },
  row: {
    flexDirection: 'row',
    marginVertical: scale(4),
  },
  txContent: {
    fontSize: 14,
    fontWeight: '400',
    lineHeight: 20,
    flex: 1,
  },
  txSubTitle: {
    fontSize: 16,
    fontWeight: '600',
    lineHeight: 20,
  },
  txTitle: {
    fontSize: 14,
    fontWeight: '600',
    lineHeight: 21,
    color: '#20419B',
  },
  containerItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  item1: {
    flex: 1,
  },
  containerValue1: {
    justifyContent: 'center',
    fontWeight: '400',
    fontSize: 14,
    lineHeight: 20,
    color: '#23262F',
  },
  containerValue1Title: {
    justifyContent: 'center',
    fontWeight: '600',
    fontSize: 14,
    lineHeight: 20,
    color: '#23262F',
  },
  containerValue2: {
    justifyContent: 'center',
    fontWeight: '400',
    fontSize: 14,
    lineHeight: 20,
    textAlign: 'center',
    color: '#23262F',
  },
  containerValue2Title: {
    justifyContent: 'center',
    fontWeight: '600',
    fontSize: 14,
    lineHeight: 20,
    textAlign: 'center',
    color: '#23262F',
  },
  containerValue: {
    flex: 1,
    marginHorizontal: scale(4),
  },
  boderBotMore: {
    borderWidth: 1,
    padding: 0,
    borderColor: '#C3CAE7',
    marginTop: scale(16),
  },
});
