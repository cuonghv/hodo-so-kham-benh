import {StyleSheet, Text, View, TouchableOpacity, Linking} from 'react-native';
import React, {useState} from 'react';
import {observer} from 'mobx-react-lite';
import {useNavigation} from '@react-navigation/native';
import {useStores} from '../../../models';
import {checkFile} from '../../../utils/healthReacord';
import {scale} from '../../../themes';
import {SvgIcon} from '../../../themes/svg-icon';
import {Routes} from '../../../helper';

const OtherIndications = observer(() => {
  const {
    healthRecord: {otherIndications},
  } = useStores();
  const [more, setMore] = useState(false);
  const navigate = useNavigation();

  const handleOpenLink = (link: string, title: any) => {
    if (link) {
      checkFile(link) === 'pdf'
        ? navigate.navigate(Routes.VIEW_PDF, {link, title})
        : Linking.openURL(link);
    }
  };

  return (
    <>
      <TouchableOpacity
        style={more ? styles.container2 : styles.container}
        onPress={() => setMore(!more)}>
        <Text style={styles.txTitle}>{'Chỉ định khác'}</Text>
        <SvgIcon icon={more ? 'arrowDown' : 'arrow_right_single'} size={14} />
      </TouchableOpacity>

      <>
        {otherIndications?.map(item => {
          return (
            <View key={item.id}>
              {more && item?.examination_result?.examination_result_values && (
                <>
                  <TouchableOpacity
                    onPress={() =>
                      handleOpenLink(item?.document_link, item?.name)
                    }>
                    <View
                      style={{
                        marginTop: scale(8),
                      }}>
                      <Text style={styles.txNameAssay}>{item?.name}</Text>
                    </View>
                  </TouchableOpacity>
                  <>
                    <View style={styles.containerTx}>
                      <View style={styles.row1}>
                        <Text style={styles.txContent}>
                          {'Các tệp kết quả'}:{' '}
                        </Text>
                        {!(item?.indication_files?.length > 0) && (
                          <Text style={styles.txContent2}>({'Chưa có'})</Text>
                        )}
                      </View>
                    </View>
                    {item?.indication_files.map(_file => {
                      return (
                        <>
                          <TouchableOpacity
                            onPress={() =>
                              handleOpenLink(
                                _file?.file_name || _file?.url,
                                _file?.name,
                              )
                            }>
                            <View style={styles.row}>
                              <SvgIcon icon="attachment" />
                              <Text key={_file.id} style={styles.txNameFile}>
                                {_file?.name || _file?.file_name}
                              </Text>
                            </View>
                          </TouchableOpacity>
                        </>
                      );
                    })}

                    {item?.examination_result?.examination_result_values?.map(
                      _detail => {
                        return (
                          <View>
                            <View style={styles.containerTx}>
                              <View style={styles.row1}>
                                <Text style={styles.txContent}>
                                  {'Chưa có'}:{' '}
                                </Text>
                                {!item?.examination_result?.description && (
                                  <Text style={styles.txContent2}>
                                    ({'Chưa có'})
                                  </Text>
                                )}
                              </View>
                              {!!item?.examination_result?.description && (
                                <Text style={styles.txCnt}>
                                  {item?.examination_result?.description}
                                </Text>
                              )}
                            </View>
                            <View style={styles.containerTx}>
                              <View style={styles.row1}>
                                <Text style={styles.txContent}>
                                  {'Kết quả chẩn đoán'}:
                                </Text>
                                {!_detail?.value && (
                                  <Text style={styles.txContent2}>
                                    ({'Chưa có'})
                                  </Text>
                                )}
                              </View>
                              {!!_detail?.value && (
                                <View>
                                  <Text>{_detail?.value}</Text>
                                </View>
                              )}
                            </View>
                          </View>
                        );
                      },
                    )}
                    <View style={styles.containerTx}>
                      <View style={styles.row1}>
                        <Text style={styles.txContent}>{'Kết luận'}:</Text>
                        {!item?.examination_result?.conclusion_result && (
                          <Text style={styles.txContent2}>({'Chưa có'}) </Text>
                        )}
                      </View>
                      <Text style={styles.txCnt}>
                        {item?.examination_result?.conclusion_result}
                      </Text>
                    </View>
                  </>
                </>
              )}
            </View>
          );
        })}
      </>
      <View style={more ? styles.boderBotMore : styles.boderBot} />
    </>
  );
});

export default OtherIndications;

const styles = StyleSheet.create({
  row1: {
    flexDirection: 'row',
  },
  txNameFile: {
    fontWeight: '500',
    fontSize: 14,
    lineHeight: 21,
    color: '#20419B',
    marginLeft: scale(8),
    paddingRight: scale(16),
  },
  containerTx: {
    marginVertical: scale(5),
  },
  txContent: {
    color: '#23262F',
    fontSize: 14,
    fontWeight: '500',
    lineHeight: 21,
  },
  txContent2: {
    color: '#7F7F7F',
    fontSize: 14,
    fontWeight: '500',
    lineHeight: 21,
  },
  txNameAssay: {
    color: '#20419B',
    flex: 1,
    fontWeight: '500',
    fontSize: 14,
    lineHeight: 21,
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingRight: scale(8),
    alignItems: 'center',
    paddingVertical: scale(8),
  },
  boderBot: {
    borderWidth: 1,
    padding: 0,
    borderColor: '#C3CAE7',
  },
  row: {
    flexDirection: 'row',
    marginVertical: scale(4),
  },

  txTitle: {
    fontSize: 14,
    fontWeight: '600',
    lineHeight: 21,
    color: '#20419B',
  },
  containerItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: scale(5),
  },
  item1: {
    flex: 1,
  },
  containerValue1: {
    justifyContent: 'center',
    fontWeight: '400',
    fontSize: 14,
    lineHeight: 20,
    color: '#23262F',
  },
  containerValue2: {
    justifyContent: 'center',
    fontWeight: '400',
    fontSize: 14,
    lineHeight: 20,
    textAlign: 'center',
    color: '#23262F',
  },
  containerValue: {
    flex: 1,
    marginHorizontal: scale(4),
  },
  txCnt: {
    fontWeight: '400',
    fontSize: 14,
    lineHeight: 21,
  },
  boderBotMore: {
    borderWidth: 1,
    padding: 0,
    borderColor: '#C3CAE7',
    marginTop: scale(16),
  },
  container2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingRight: scale(8),
    alignItems: 'center',
    paddingTop: scale(8),
  },
});
