import {StyleSheet, View} from 'react-native';
import React from 'react';
import {observer} from 'mobx-react-lite';
import PrescriptionItem from './PrescriptionItem';
import {useStores} from '../../../models';

const Prescription = observer(() => {
  const {
    healthRecord: {prescription},
  } = useStores();

  return (
    <>
      <>
        {prescription?.map(item => {
          return <PrescriptionItem item={item} key={item.id} />;
        })}
      </>
      <View style={styles.boderBot} />
    </>
  );
});

export default Prescription;

const styles = StyleSheet.create({
  boderBot: {
    borderWidth: 1,
    padding: 0,
    borderColor: '#C3CAE7',
  },
});
