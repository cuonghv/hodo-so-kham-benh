import {SafeAreaView, StyleSheet} from 'react-native';
import React from 'react';
import Pdf from 'react-native-pdf';
import Header from '../components/header/header';

const ViewPdf = ({route}: any) => {
  const {title, link} = route?.params;

  return (
    <SafeAreaView style={styles.root}>
      <Header iconBack title={title} />
      <Pdf
        source={{
          uri: link ?? '',
          cache: true,
        }}
        style={styles.root}
      />
    </SafeAreaView>
  );
};

export default ViewPdf;

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
});
