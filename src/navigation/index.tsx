import 'react-native-gesture-handler';
import React, {useEffect, useState} from 'react';
import {AppRegistry, StyleSheet} from 'react-native';
import {Host} from 'react-native-portalize';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import {
  SafeAreaProvider,
  initialWindowMetrics,
} from 'react-native-safe-area-context';
import AppNavigation from './AppNavigation';
import {initFonts} from '../themes/fonts';
import {RootStore, RootStoreProvider, setupRootStore} from '../models';
import {ENV} from '../types/types';
import {EnumTypeHealthRecord} from '../utils/constant';

interface IProps {
  token: string;
  personId: number;
  personDiseasesId: number;
  env: ENV;
  typeGetData: EnumTypeHealthRecord;
}

export default (props: IProps) => {
  const {token, personId, personDiseasesId, env, typeGetData} = props;

  const [rootStore, setRootStore] = useState<RootStore | undefined>(undefined);
  useEffect(() => {
    initialSetup();
  }, []);

  const initialSetup = async () => {
    await initFonts(); // expo
    const store = await setupRootStore();
    setRootStore(store);
  };
  if (!!rootStore) {
    return (
      <RootStoreProvider value={rootStore}>
        <SafeAreaProvider initialMetrics={initialWindowMetrics}>
          <GestureHandlerRootView style={styles.container}>
            <Host>
              <AppNavigation
                token={token}
                personId={personId}
                personDiseasesId={personDiseasesId}
                env={env}
                typeGetData={typeGetData}
              />
            </Host>
          </GestureHandlerRootView>
        </SafeAreaProvider>
      </RootStoreProvider>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
