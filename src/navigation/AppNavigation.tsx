import React, {useEffect} from 'react';
import {Header, createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import {Routes, navigationRef} from '../helper';
import {HealthRecords} from '../screens';
import {useStores} from '../models';

import MedicalSession from '../screens/MedicalSessionScreen';
import ViewPdf from '../screens/ViewPDFScreen';
import {ENV} from '../types/types';
import {EnumTypeHealthRecord} from '../utils/constant';

const Stack = createStackNavigator();

interface IProps {
  token: string;
  personId: number;
  personDiseasesId: number;
  env: ENV;
  typeGetData: EnumTypeHealthRecord;
}

const AppNavigation = (props: IProps) => {
  const {token, personId, personDiseasesId, env, typeGetData} = props;
  const {setting} = useStores();

  if (token) setting.setToken(token);
  if (personId) setting.setPersonId(personId);
  if (personDiseasesId) setting.setPersonDiseaseId(personDiseasesId);
  if (env) setting.setEnv(env);
  if (typeGetData) setting.setTypeGetData(typeGetData);

  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator
        initialRouteName={Routes.HOME}
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen component={HealthRecords} name={Routes.HOME} />
        <Stack.Screen component={MedicalSession} name={Routes.DETAIL} />
        <Stack.Screen component={ViewPdf} name={Routes.VIEW_PDF} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigation;
