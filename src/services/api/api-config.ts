// Use this import if you want to use "env.js" file
// const { API_URL } = require("../../config/env")
// Or just specify it directly like this:

/**
 * The options used to configure the API.
 */
export interface ApiConfig {
  /**
   * The URL of the api.
   */
  url: string;

  /**
   * Milliseconds before we timeout the request.
   */
  timeout: number;
}

const URL_API_HIDO02_PRD = 'https://hid02.hodo.app';
const URL_API_DEV_HIDO2 = 'https://dev.hid02.hodo.app';

export const URL_API_HIDO02 = __DEV__ ? URL_API_DEV_HIDO2 : URL_API_HIDO02_PRD;

export const DEV_API_CONFIG = {
  appUrl: 'https://dev.hodo.app',
  baseURL: 'https://api-dev.hodo.app',
  baseTwilioVoiceURL: 'https://api-node.demo.hodo.app',
};

export const PROD_API_CONFIG = {
  appUrl: 'https://hodo.app',
  baseURL: 'https://api.hodo.app',
  baseTwilioVoiceURL: 'https://api-node.demo.hodo.app',
};
/**
 * The default configuration for the app.
 */
export const DEFAULT_API_CONFIG: ApiConfig = {
  url: __DEV__ ? DEV_API_CONFIG.baseURL : PROD_API_CONFIG.baseURL,
  timeout: 60000,
};

export const STORAGE_URL = 'storage';
