export const USER = {
  getMe: () => '/api/go/core/auth/me',
  getMedicalBooks: () => '/api/auth/health-records?limit=10000',

  getMedicalBooksGo: (param: any) =>
    `/api/go/health_profiles/health_records?person_id=${param.person_id}&person_diseases_id=${param.person_diseases_id}&page_num=${param?.page_num}&page_size=${param?.page_size}&order=desc`,

  getDetailMedicalBookGo: (param: any) =>
    `/api/go/health_profiles/health_records/patient_visit?person_id=${param.person_id}&person_diseases_id=${param.person_diseases_id}&page_num=${param?.page_num}&page_size=${param?.page_size}`,

  getPatientVisitDetailGo: (id: number) =>
    `/api/go/health_profiles/health_records/patient_visit/${id}`,

  getDetailMedicalBook: (id: number) =>
    `/api/auth/health-records/${id}/patientvisit?limit=10000`,
};
