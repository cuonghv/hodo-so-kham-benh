import {ApisauceInstance, create} from 'apisauce';
import {
  ApiConfig,
  STORAGE_URL,
  DEFAULT_API_CONFIG,
  DEV_API_CONFIG,
  PROD_API_CONFIG,
} from './api-config';

/**
 * Manages all requests to the API.
 */
export class Api {
  /**
   * The underlying apisauce instance which performs the requests.
   */
  apisauce: ApisauceInstance;

  /**
   * Configurable options.
   */
  config: ApiConfig;

  queue: any[];

  /**
   * Creates the api.
   *
   * @param config The configuration to use.
   */
  constructor(config: ApiConfig = DEFAULT_API_CONFIG) {
    this.config = config;
  }

  /**
   * Sets up the API.  This will be called during the bootup
   * sequence and will happen before the first React component
   * is mounted.
   *
   * Be as quick as possible in here.
   */
  setup() {
    // construct the apisauce instance
    this.apisauce = create({
      baseURL: this.config.url,
      timeout: this.config.timeout,
      headers: {
        Accept: 'application/json',
      },
    });
    this.queue = [];
  }

  switchDev() {
    this.apisauce.setBaseURL(DEV_API_CONFIG.baseURL);
  }

  switchProd() {
    this.apisauce.setBaseURL(PROD_API_CONFIG.baseURL);
  }

  getImageUri(path) {
    return {
      uri: `${this.apisauce.getBaseURL()}/${STORAGE_URL}/${path}`,
    };
  }

  getAppUri(path, devMode?) {
    const url = devMode ? DEV_API_CONFIG.appUrl : PROD_API_CONFIG.appUrl;
    return {
      uri:
        path.includes('http') || path.includes('https')
          ? path
          : `${url}/${path}`,
    };
  }
}
