import axios from 'axios';
import {DEV_API_CONFIG, PROD_API_CONFIG} from './api-config';
import {useStores} from '../../models';
import {EnumEnv} from '../../utils/constant';

export const HealthRecordApi = async ({url, method, token}) => {
  const {setting} = useStores();

  const options: any = {
    url:
      setting.env === EnumEnv.DEV
        ? DEV_API_CONFIG.baseURL + url
        : PROD_API_CONFIG.baseURL + url,
    method: method,
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  };

  return axios(options);
};
