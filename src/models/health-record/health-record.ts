import {withEnvironment} from './../extensions/with-environment';
import {Instance, SnapshotOut, types} from 'mobx-state-tree';

/**
 * Model description here for TypeScript hints.
 */
const Model = types.model({
  healthRecordList: types.frozen(),
  chooseHealthRecord: types.frozen(),
  healthRecordDetail: types.frozen(),
  chooseHealthRecordDetail: types.frozen(),
  patientVisitDetail: types.frozen(),
  consultation: types.frozen(),
  prescription: types.frozen(),
  files: types.frozen(),
  assay: types.frozen(),
  diagnoseYourImage: types.frozen(),
  otherIndications: types.frozen(),
  molecularBiology: types.frozen(),
});

export const HealthRecordModel = Model.named('HealthRecord')
  .extend(withEnvironment)
  .actions(self => ({
    setHealthRecordList(data: any) {
      self.healthRecordList = data;
    },
    setChooseHealthRecord(data: any) {
      self.chooseHealthRecord = data;
    },
    setHealthRecordDetail(data: any) {
      self.healthRecordDetail = data;
    },
    setChooseHealthRecordDetail(data: any) {
      self.chooseHealthRecordDetail = data;
    },
    setPatientVisitDetail(data: any) {
      self.patientVisitDetail = data;
    },
    setConsultation(data: any) {
      self.consultation = data;
    },
    setPrescription(data: any) {
      self.prescription = data;
    },
    setFile(data: any) {
      self.files = data;
    },
    setAssay(data: any) {
      self.assay = data;
    },
    setDiagnoseYourImage(data: any) {
      self.diagnoseYourImage = data;
    },
    setOtherIndication(data: any) {
      self.otherIndications = data;
    },
    setMolecularBiology(data: any) {
      self.molecularBiology = data;
    },
  }));

export type HealthRecord = Instance<typeof HealthRecordModel>;
export type HealthRecordSnapshot = SnapshotOut<typeof HealthRecordModel>;
