import {onSnapshot} from 'mobx-state-tree';
import {RootStoreModel, RootStore} from './root-store';
import {Environment} from '../environment';
import * as storage from '../../utils/storage';
import {ApiResponse} from 'apisauce';
import makeInspectable from 'mobx-devtools-mst';
import {EventRegister} from 'react-native-event-listeners';
import {showErrorToast} from '../../utils';

/**
 * The key we'll be saving our state as within async storage.
 */
const ROOT_STATE_STORAGE_KEY = 'root';

/**
 * Setup the environment that all the models will be sharing.
 *
 * The environment includes other functions that will be picked from some
 * of the models that get created later. This is how we loosly couple things
 * like events between models.
 */
export async function createEnvironment() {
  const env = new Environment();
  await env.setup();
  return env;
}

// export function handleError<T extends BaseResponse<T> = any>(
//   response: ApiResponse<T>,
// ) {
//   if (!response.problem) {
//     return;
//   }
//   const showMessage = (title: string, content: string) => {
//     response.ok = false;
//     showErrorToast({
//       text1: title,
//       text2: content,
//     });
//   };

//   if (response.status === 401) {
//     EventRegister.emit('INVALID_TOKEN');
//     return;
//   }

//   switch (response.problem) {
//     case 'NETWORK_ERROR':
//       showMessage(
//         'Không thành công',
//         'Vui lòng kiểm tra kết nối mạng và thử lại!',
//       );
//       break;
//     default:
//       if (
//         response.problem !== null ||
//         !response.ok ||
//         response.data?.status !== 'success'
//       ) {
//         showMessage(
//           'Không thành công',
//           response?.data?.message || 'Có lỗi xảy ra, vui lòng thử lại sau!',
//         );
//       }
//   }
// }

/**
 * Setup the root state.
 */
export async function setupRootStore() {
  let rootStore: RootStore;
  let data: any;

  // prepare the environment that will be associated with the RootStore.
  const env = await createEnvironment();
  try {
    // load data from storage
    data = (await storage.load(ROOT_STATE_STORAGE_KEY)) || {};
    rootStore = RootStoreModel.create(data, env);
  } catch (e) {
    // if there's any problems loading, then let's at least fallback to an empty state
    // instead of crashing.
    rootStore = RootStoreModel.create({}, env);

    // but please inform us what happened
    __DEV__ && console.tron.error(e.message, null);
  } finally {
    makeInspectable(rootStore);
  }

  // reactotron logging
  if (__DEV__) {
    env.reactotron.setRootStore(rootStore, data);
  }

  // track changes & save to storage
  onSnapshot(rootStore, snapshot =>
    storage.save(ROOT_STATE_STORAGE_KEY, snapshot),
  );

  env.api.apisauce.addRequestTransform(request => {
    env.api.queue.push(undefined);
    const token = rootStore?.user?.token;
    token && (request.headers['Authorization'] = `Bearer ${token}`);
    //QUYETLV unset loading when update setting in root store
    !rootStore.setting.loading && rootStore.setting.setLoading(false);
  });

  env.api.apisauce.addResponseTransform(response => {
    __DEV__ &&
      console.log(
        `${response.config.baseURL}/${response.config.url}`,
        response?.data,
      );

    env.api.queue.pop();
    rootStore.setting.setLoading(false);

    handleError(response);
  });

  return rootStore;
}
