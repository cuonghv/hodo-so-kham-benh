import {applySnapshot, Instance, SnapshotOut, types} from 'mobx-state-tree';
import {HealthRecordModel} from '../health-record/health-record';
import {SettingModel} from '../setting/setting';

/**
 * A RootStore model.
 */
// prettier-ignore
export const RootStoreModel = types.model('RootStore').props({
  healthRecord: types.optional(HealthRecordModel, {} as any),
  setting: types.optional(SettingModel, {} as any),
});
/**
 * The RootStore instance.
 */
export type RootStore = Instance<typeof RootStoreModel>;

/**
 * The data of a RootStore.
 */
export type RootStoreSnapshot = SnapshotOut<typeof RootStoreModel>;
