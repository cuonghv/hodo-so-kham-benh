import {flow, Instance, SnapshotOut, types} from 'mobx-state-tree';
import {withEnvironment} from '../extensions/with-environment';
import {EdgeInsets} from 'react-native-safe-area-context';
import {EnumTypeHealthRecord} from '../../utils/constant';
import {ENV, TypeHealthRecord} from '../../types/types';

const InsetsModel = types.model({
  top: 0,
  bottom: 0,
  right: 0,
  left: 0,
});
/**
 * Model description here for TypeScript hints.
 */
export const SettingModel = types
  .model('Setting')
  .props({
    loading: false,
    online: true,
    version: '',
    devMode: __DEV__,
    insets: types.optional(InsetsModel, {
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
    }),
    skipped: false,
    diseaseId: 0,
    invite: -1,
    sttModalUpdateCSSH: false,
    token: '',
    typeGetData: EnumTypeHealthRecord.HEALTH_RECORD_ALL,
    personId: 0,
    personDiseasesId: 0,
    env: '',
  })
  .extend(withEnvironment)

  .views(self => ({
    get top(): number {
      return self.online ? self.insets.top : 0;
    },
  }))
  .actions(self => ({
    setEnv(env: ENV) {
      self.env = env;
    },
    setToken(token: string) {
      self.token = token;
    },
    setTypeGetData(type: TypeHealthRecord) {
      self.typeGetData = type;
    },
    setPersonId(personId: number) {
      self.personId = personId;
    },
    setPersonDiseaseId(personDiseasesId: number) {
      self.personDiseasesId = personDiseasesId;
    },
    setDiseaseId: (id: number) => {
      self.diseaseId = id;
    },
    setSkipped: (value: boolean) => {
      self.skipped = value;
    },
    setInvite: (value: number) => {
      self.invite = value;
    },
    setOnline: (value: boolean) => {
      self.online = value;
    },
    setInsets: (value: EdgeInsets) => {
      self.insets.top = Math.max(value.top, self.insets.top);
      self.insets.bottom = Math.max(value.bottom, self.insets.bottom);
      self.insets.left = Math.max(value.left, self.insets.left);
      self.insets.right = Math.max(value.right, self.insets.right);
    },
    setLoading: (value: boolean) => {
      self.loading = value;
    },
    getAppUri: (path: any) => {
      return self.environment.api.getAppUri(path, self.devMode);
    },
    switchDevMode: (value: boolean) => {
      self.devMode = value;
      if (value == true) {
        self.environment.api.switchDev();
      } else {
        self.environment.api.switchProd();
      }
    },
    connectStringeeToken: flow(function* () {
      const api = new StringeeApi(self.environment.api);
      const response: Kind<any> = yield api.requests.connectToken();
      return response;
    }),
    sendCallSignal: flow(function* (receiverId: number, callerName: string) {
      const api = new StringeeApi(self.environment.api);
      const response: Kind<any> = yield api.requests.sendCallSignal({
        user_id: receiverId,
        caller_name: callerName,
      });
      return response;
    }),
    setSttModalUpdateCSSH(stt: boolean) {
      self.sttModalUpdateCSSH = stt;
    },
    // postDevice: flow(function* (
    //   deviceType: string,
    //   deviceId: number,
    //   deviceToken: string,
    // ) {
    //   const api = new NotificationApi(self.environment.api)
    //   const response: Kind<any> = yield api.requests.postDevice({
    //     device_type: deviceType,
    //     device_id: deviceId,
    //     device_token: deviceToken,
    //   })
    //   return response
    // })
  }));

export type Setting = Instance<typeof SettingModel>;
export type SettingSnapshot = SnapshotOut<typeof SettingModel>;
export const createSettingDefaultModel = () => types.optional(SettingModel, {});
