import {getEnv, IStateTreeNode} from 'mobx-state-tree';
import {Environment} from '../environment';
import {showErrorToast, showToast} from '../../utils/toast';

/**
 * Adds a environment property to the node for accessing our
 * Environment in strongly typed.
 */
export const withEnvironment = (self: IStateTreeNode) => ({
  views: {
    /**
     * The environment.
     */
    get environment() {
      return getEnv<Environment>(self);
    },
  },
  actions: {
    showToast: (message: string, type: 'success' | 'error' = 'success') => {
      if (!message) {
        return;
      }
      switch (type) {
        case 'success':
          showToast({
            text1: message,
          });
          break;
        case 'error':
          showErrorToast({
            text1: message,
          });
          break;
      }
    },
  },
});
