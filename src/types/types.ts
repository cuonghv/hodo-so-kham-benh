export type TypeModal = 'phien_kham' | 'so_kham';

export type TypeHealthRecord = 'health_record_all' | 'health_record_for_HSSK';

export type ENV = 'dev' | 'prd';
