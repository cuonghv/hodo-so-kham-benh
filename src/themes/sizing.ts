import {Dimensions, PixelRatio} from 'react-native';

const {width, height} = Dimensions.get('window');

const baseWidth = 375;
const guidelineBaseHeight = 812;

export const scale = (size: number) => {
  return (height / guidelineBaseHeight) * size;
};

export const scaleWidth = (widthPercent: number) => {
  const elemWidth =
    typeof widthPercent === 'number' ? widthPercent : parseFloat(widthPercent);

  // Use PixelRatio.roundToNearestPixel method in order to round the layout
  // size (dp) to the nearest one that correspons to an integer number of pixels.
  return PixelRatio.roundToNearestPixel((width * elemWidth) / 100);
};

export const scaleHeight = (heightPercent: number) => {
  // Parse string percentage input and convert it to number.
  const elemHeight =
    typeof heightPercent === 'number'
      ? heightPercent
      : parseFloat(heightPercent);

  // Use PixelRatio.roundToNearestPixel method in order to round the layout
  // size (dp) to the nearest one that correspons to an integer number of pixels.
  return PixelRatio.roundToNearestPixel((height * elemHeight) / 100);
};

// export const scaleSize = (size) => Math.ceil(size * scale)
export const scaleSize = (size: number) => size;
export const screen = {width, height};
