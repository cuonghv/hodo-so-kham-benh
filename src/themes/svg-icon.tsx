import * as React from 'react';
import {StyleProp, ViewStyle} from 'react-native';
import {SVGIconTypes} from './icon-list';
import {icons} from './icon-list';
import {SvgProps} from 'react-native-svg';

export interface SvgIconProps extends SvgProps {
  style?: StyleProp<ViewStyle>;
  icon: SVGIconTypes;
  height?: number | string;
  width?: number | string;
  size?: number | string;
}

export function SvgIcon(props: SvgIconProps) {
  const {style, icon, size} = props;
  let {height, width} = props;
  if (size) {
    height = size;
    width = size;
  }

  const IconComponent = icons[icon];
  if (!IconComponent) {
    return null;
  }
  return (
    <IconComponent
      {...props}
      height={height || 20}
      width={width || 20}
      style={style || {}}
    />
  );
}
